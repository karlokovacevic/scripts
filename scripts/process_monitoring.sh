#!/bin/bash

while : # : is bash built in command that always returns true
do
	output="$(pgrep -l $1)"
	if [[ -n "$output" ]]
	then
		echo "The process \"$1\" is running." # \...\ is used for displaying double quotes in output
	else
		echo "The process \"$1\" is NOT running."
	fi
	sleep 3 
done 
