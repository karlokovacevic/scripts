#!/bin/bash

dropped_ips="$(cat $1)"

for ip in $dropped_ips
do
	echo "Dropping packets from $ip"
	iptables -I INPUT -s $ip -j DROP
done
