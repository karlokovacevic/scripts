#!/bin/bash

# Write the iptables commands that flush all the tables of all chains and set the ACCEPT policy on all chains. This will delete any firewall.

# flushing
iptables -t filter -F
iptables -t nat -F
iptables -t mangle -F
iptables -t raw -F 

# default policy to ACCEPT
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
