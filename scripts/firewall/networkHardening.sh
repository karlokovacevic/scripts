#!/bin/bash

# Create a firewall script for your Laptop that runs Linux. All outgoing traffic is allowed but only the return incoming traffic is permitted. No services are running on the laptop.

iptables -F

# stateful
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT 

iptables -P INPUT DROP
iptables -P OUTPUT DROP 
