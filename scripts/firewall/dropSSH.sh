#!/bin/bash

# Write an iptables rule that drops all incoming packets to port 22/tcp (ssh). This should be the first rule in the chain.

iptables -I INPUT -p tcp --dport 22 -j DROP
