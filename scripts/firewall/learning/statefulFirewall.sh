#!/bin/bash

# flush filter table 
iptables -F

# allow loopback interface traffic
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT 

# stateful part 

# allowing SSH from specific host
# iptables -A INPUT -p tcp --dport 22 -m state --state NEW -s 192.168.0.1 -j ACCEPT 

# dropping packets with invalid packet state
iptables -A INPUT -m state --state INVALID -j DROP
iptables -A OUTPUT -m state --state INVALID -j DROP

# allowing establishing new connections for outgoing traffic and for incoming part allowing traffic only from existing connections
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

# changing default policy to drop packets that don't fulfill given rules
iptables -P INPUT DROP
iptables -P OUTPUT DROP
