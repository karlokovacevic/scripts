#!/bin/bash

if [[ $# -eq 1 ]]
then
	iptables -F INPUT

	macs="$(cat $1)"

	for mac in $macs
	do
		iptables -A INPUT -m mac --mac-source $mac -j ACCEPT
		echo "$mac permitted"
	done

	iptables -P INPUT DROP
else
	echo "Please provide only one file as an argument."
fi
