#!/bin/bash

read -p "Enter the IP address of machine that will be allowed to SSH to this machine: " ip

iptables -F

iptables -A INPUT -p tcp --dport 22 -s $ip -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -s 0/0 -j DROP # 0/0 representing all IP addresses

# disallow HTTP and HTTPS traffic
iptables -A INPUT -p tcp -m multiport --dports 80,443 -j DROP
