#!/bin/bash

iptables -F
# dropping incoming ssh traffic
iptables -A INPUT -p tcp --dport 22 -j DROP

# dropping outgoing http and https traffic
iptables -A OUTPUT -p tcp --dport 80 -j DROP
iptables -A OUTPUT -p tcp --dport 443 -j DROP
