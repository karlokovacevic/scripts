#!/bin/bash

# flushing old rules
iptables -F

# accept SSH traffic between 3AM and 5AM, with assumption that default policy on INPUT chain is set to ACCEPT
iptables -A INPUT -p tcp --dport 22 -m time --timestart 3:00 --timestop 5:00 -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -j DROP

# allow specific HTTPS traffic after working hours, we suppose that device is router, with assumption that default policy on chain is set to ACCEPT
iptables -A FORWARD -p tcp --dport 443 -d www.ubuntu.com -m time --timestart 18:00 --timestop 8:00 -j ACCEPT
iptables -A FORWARD -p tcp --dport 443 -d www.ubuntu.com -j DROP
