#!/bin/bash

# Write the iptables rules that drop all outgoing packets of type tcp (port 80 and 443) to www.linuxquestions.org
# The Linux machine is the router.

iptables -A FORWARD -p tcp -m multiport --dports 80,443 -d www.linuxquestions.org -j DROP
