#!/bin/bash

# Write an iptables rule that drops all incoming packets from network 27.103.0.0 255.255.0.0
# This will be the first rule in the chain.

iptables -I INPUT -s 27.103.0.0/16 -j DROP
