#!/bin/bash

# Write the iptables rules that permit outgoing web traffic (tcp ports 80 and 443) only between 10:00 and 18:00 UTC.
# Add a match to allow web traffic only on the weekend between 10:00 and 18:00 UTC.

iptables -A OUTPUT -p tcp -m multiport --dports 80,443 -m time --timestart 10:00 --timestop 18:00 --weekdays Sat,Sun -j ACCEPT
iptables -A OUTPUT -p tcp -m multiport --dports 80,443 -j DROP
