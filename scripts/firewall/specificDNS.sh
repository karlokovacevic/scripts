#!/bin/bash

# The DNS Server of your LAN is set to 8.8.8.8. You don't want to allow the users of the LAN to change the DNS server.
# Write an iptables rule in order to drop all UDP packets to port 53 (DNS) if they are destined to another IP address (not to 8.8.8.8). The Linux Machine is the Router.

iptables -A FORWARD -p udp --dport 53 ! -d 8.8.8.8 -j DROP
