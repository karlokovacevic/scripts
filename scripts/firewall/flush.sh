#!/bin/bash

# Write the iptables command that flushes the filter table of all chains.

iptables -F # filter table is default table so there is no need for explicit option provision 
# or
iptables -t filter -F
