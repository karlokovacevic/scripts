#!/bin/bash

# Write the iptables rules that permit outgoing web traffic (tcp ports 80 and 443) only between 10:00 and 18:00 UTC.

iptables -A OUTPUT -p tcp -m multiport --dports 80,443 -m time --timestart 10:00 --timestop 18:00 -j ACCEPT
iptables -A OUTPUT -p tcp -m multiport --dports 80,443 -j DROP
