#!/bin/bash

# Write the iptables commands that set the default POLICY to ACCEPT on INPUT and OUTPUT chains and DROP on FORWARD chain.

iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD DROP
