#!/bin/bash

# The MAC Address of the LAN Router is b4:6d:83:77:85:f5
# Write a single iptables rule that allows the communication of your Linux host only with the router. It cannot communicate with other hosts inside the same LAN. Do not modify the policy.

iptables -A INPUT -m mac ! --mac-source b4:6d:83:77:85:f5 -j DROP 
