#!/bin/bash

# Write the iptables rules that drop all incoming packets from 100.0.0.1 and 1.2.3.4 and all outgoing packets to 80.0.0.1
# These will be the first rules in the chains.

iptables -I INPUT -s 100.0.0.1 -j DROP
iptables -I INPUT -s 1.2.3.4 -j DROP

iptables -I OUTPUT -d 80.0.0.1 -j DROP
