#!/bin/bash	

myStr="abc"

if [[ -z "$myStr" ]]
then
	echo "String is zero in length."
else 
	echo "String is not zero in length."
fi

if [[ -n "$myStr" ]]
then
	echo "String is not zero in length."
else
	echo "String is zero in length."
fi

