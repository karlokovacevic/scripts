#!/bin/bash	

function printSomething () {
	echo "I'm a simple function!"
}

displaySomething () {
	echo "Hello functions!"
}

createFiles () {
	echo "Creating $1"
	touch $1
	chmod 400 $1
	echo "Creating $2"
	touch $2
	chmod 600 $2
	return 10	
}

function linesInFile () {
	grep -c "$1" "$2" # -c option for final amount of occurences
}

printSomething
displaySomething

createFiles aa.txt bb.txt
echo $? # it will print number 10, because of the return in function

n=$(linesInFile "usb" "/var/log/dmesg") # command substitution as replacement for return in function
echo $n




