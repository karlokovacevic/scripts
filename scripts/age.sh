#!/bin/bash
read -p "Enter your age: " age

if [[ $age -lt 18 ]] && [[ $age -ge 0 ]]
then
	echo "You are minor!"
elif [[ $age -eq 18 ]]
then
	echo "Congratulations! You are not minor anymore!"
elif [[ $age -gt 18 ]] && [[ $age -le 100 ]]
then 
	echo "You are an adult."
else
	echo "Invalid age!"
fi 
