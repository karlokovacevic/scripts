#!/bin/bash

dropped_ips="8.8.8.8 1.1.1.1 10.10.10.1"

for ip in $dropped_ips
do
	echo "Dropping packets from $ip"
	iptables -I INPUT -s $ip -j DROP
done
