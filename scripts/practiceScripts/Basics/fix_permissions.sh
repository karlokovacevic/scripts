#!/bin/bash

read -p "Enter the directory for which you want to change permissions: " dir

echo "Chaning subdirectories permissions to 755 recursively..."
find $dir -type d -exec chmod 755 {} \;
echo "Done"

echo "Changing files permissions inside directory to 644 recursively..."
find $dir -type f -exec chmod 644 {} \;
echo "Done"
