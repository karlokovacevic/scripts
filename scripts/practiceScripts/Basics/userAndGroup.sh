#!/bin/bash

read -p "Enter the username for the new user: " username
read -p "Enter the name of the group that will be created: " group

groupadd $group
useradd -m -s /bin/bash -G $group $username

tail -n 2 /etc/group
echo
tail -n 2 /etc/passwd
echo

echo "The user $username was created and he is a member of new group $group!"
 
