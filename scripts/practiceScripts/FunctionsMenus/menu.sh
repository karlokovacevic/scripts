#!/bin/bash
PS3="Choose: " # Changing prompt

select ITEM in "Display date and time" "List users logged in" "Display disk usage" "Quit"
do
	if [[ $REPLY -eq 1 ]]
	then
		echo "Displaying date and time."
		date 
	elif [[ $REPLY -eq 2 ]]
	then
		echo "Displaying logged in users."
		who -aH
	elif [[ $REPLY -eq 3 ]]
	then
		echo "Displaying disk usage."
		df -h
	elif [[ $REPLY -eq 4 ]]
	then
		echo "Quitting..."
		sleep 1
		exit
	else
		echo "Unknown selection."
	fi
done
