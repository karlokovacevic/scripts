#!/bin/bash

rectangle_area () {
	area=$(($1*$2))
	echo "Area of rectangle with width $1 and height $2 is $area."
}

rectangle_area 3 4
rectangle_area 2 3
