#!/bin/bash
PS3="Choose: " # Changing prompt

select ITEM in "Display date and time" "List users logged in" "Display disk usage" "Quit"
do
	case $REPLY in
		1)
			echo "Displaying date and time."
			date 
			;;
		2)
			echo "Displaying logged in users."
			who -aH
			;;
		3)	
			echo "Displaying disk usage."
			df -h
			;;
		4)	
			echo "Quitting..."
			sleep 1
			exit
			;;
		*)	
			echo "Unknown selection."
			;;
	esac
done
