#!/bin/bash

user="$(who | cut -d " " -f1)"
dateTime="$(date +%F\ %H:%M)" # To escape the whitespace

echo $user >> userAndDate.txt
echo "----------------------" >> userAndDate.txt
echo $dateTime >> userAndDate.txt
