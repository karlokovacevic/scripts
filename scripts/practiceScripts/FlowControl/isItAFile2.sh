#!/bin/bash

if [[ $# -eq 1 ]]
then
	if [[ -f "$1" ]]
	then
		echo "It's a file!"
	elif [[ -d "$1" ]]
	then 
		echo "It's a directory!"
	else
		echo "Mistake!"
	fi
else
	echo "The script is run with $# arguments. You should run it with exactly one argument!"
fi
