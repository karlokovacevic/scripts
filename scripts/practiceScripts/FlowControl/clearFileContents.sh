#!/bin/bash

if [[ -f "$1" ]]
then
	echo "" > $1
	echo "$1 contents are deleted!"
else 
	"The script's argument should be a file."
fi
