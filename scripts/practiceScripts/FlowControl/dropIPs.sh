#!/bin/bash

read -p "Enter IP address to drop: " ip
iptables -I INPUT -s $ip -j DROP
echo "All packets from $ip will be dropped."
