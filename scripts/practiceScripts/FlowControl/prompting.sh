#!/bin/bash

read -p "Enter character (Y/N): " char

if [[ "$char" == "Y" ]]
then
	echo "\"YES\""
elif [[ "$char" == "N" ]]
then
	echo "\"NO\""
else
	echo "Another character. Exiting..."
	exit
fi
