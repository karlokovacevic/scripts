#!/bin/bash

read -p "Enter character (Y/N): " char

if [[ "$char" == "Y" ]] || [[ "$char" == "y" ]] 
then
	echo "\"YES\""
elif [[ "$char" == "N" ]] || [[ "$char" == "n" ]] 
then
	echo "\"NO\""
else
	echo "Another character. Exiting..."
	exit
fi
