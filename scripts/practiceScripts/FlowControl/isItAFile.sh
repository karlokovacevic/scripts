#!/bin/bash

if [[ -f "$1" ]]
then
	echo "It's a file!"
elif [[ -d "$1" ]]
then 
	echo "It's a directory!"
else
	echo "Mistake!"
fi
