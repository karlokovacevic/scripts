#!/bin/bash

if [[ $# -eq 1 ]]
then
	iptables -I INPUT -s $1 -j DROP
	echo "All packets from $1 will be dropped."
else
	echo "Run the script with the IP address as an argument."
fi
