#!/bin/bash

if [[ $# -ne 2 ]]
then
	echo "Run the script with exactly 2 arguments."
else 
	output="$($1)"
	if [[ "$output" == *"$2"* ]]
	then
		echo "$2 belongs to the output of $1"
	else
		echo "$2 doesn't belong to the output of $1"
	fi
fi
