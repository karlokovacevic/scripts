#!/bin/bash

read -p "Enter how many files do you want to create: " n

i="0"
while [[ $i -lt $n ]]
do
	touch "file$((i+1)).txt"
	((i++))
done
echo "$n files were created."
