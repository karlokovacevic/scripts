#!/bin/bash 

echo -n "Enter a file: "
read file

if [[ -f "$file" ]]
then
	read -p "Enter a number from which you want to see print: " n
	read -p "Enter a number to which you want to see print: " m
	num=$((m-n+1)) # number of lines to display

	tail -n "+$n" $file | head -n $num
else
	echo "$file is not a file."
fi
