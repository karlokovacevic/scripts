#!/bin/bash

read -p "Enter file from which script will ping addresses: " file

output="$(cat $file)"

for ip in $output
do
	ping -c 1 $ip
	echo "##################################"
done
