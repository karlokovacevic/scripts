#!/bin/bash

read -p "Enter how many files do you want to create: " n

if [[ $n -gt 0 ]] && [[ $n -lt 100 ]]
then
	i="0"
	while [[ $i -lt $n ]]
	do
		name="$(date +%M%S)"
		echo "Creating $name"
		touch "$name"
		((i++))
		sleep 3
	done
	echo "$n files were created."
else
	echo "Enter positive integer that is smaller than 100."
fi
