#!/bin/bash

for i in {1..50..2}
do
	echo "$i"
done

echo "#############"
echo "Another way" 
sleep 1

i="1"
while [[ $i -lt 50 ]]
do
	echo "$i"
	let i=i+2
done
