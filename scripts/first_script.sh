#!/bin/bash
# This is a Bash comment
mkdir -p dir1 # creating directory
echo "some text" > dir1/file.txt # creating a file with some text
ls -l dir1 # listing the directory
cat dir1/file.txt # file contents
